﻿using System;

namespace TRI
{
	class MainClass
	{
		//Struct Phòng
		public struct PHONG
		{
			public int maPhong;
			public string tenPhong;
			public int donGia;
			public int soGiuong;
			public bool phongVip;
		}

		// Struct Khách Thuê Phòng
		public struct KHACH
		{
			public string soCMND;
			public string hoTen;
			public DateTime ngayThue;
			public DateTime ngayTra;
			public int maPhong;
		}
			
		//Hàm Thêm Phòng
		public static void themPhong(PHONG[] danhSachPhong) {
			PHONG phong;
			try
			{
				Console.WriteLine ("Nhap Ma Phong:");
				phong.maPhong = int.Parse(Console.ReadLine ());

				Console.WriteLine ("Nhap Ten Phong:");
				phong.tenPhong = Console.ReadLine ();

				Console.WriteLine ("Nhap Don Gia:");
				phong.donGia = int.Parse(Console.ReadLine ());

				Console.WriteLine ("Nhap So Giuong:");
				phong.soGiuong = int.Parse(Console.ReadLine ());

				Console.WriteLine ("Co Phai La Phong Vip Khong? Nhap 1 la Vip, 0 la thuong:");
				phong.phongVip = bool.Parse(Console.ReadLine ());
				Console.WriteLine ("Ma Phong: {0}," +
					" Ten Phong: {1}," +	
					" Don Gia: {2}," +
					" So Giuong: {3}"+
					" Phong Vip: {4}"
					,phong.maPhong, phong.tenPhong, phong.donGia, phong.soGiuong, phong.phongVip);
			}
			catch
			{
				Console.WriteLine ("Vui Long Nhap Dung Dinh Dang");	
				themPhong (danhSachPhong);
			}
		}

		public static void timPhong(PHONG[] danhSachPhong, string tenPhong)
		{


		}

		//-------------------------Main--------------------------------//

		public static void Main (string[] args)
		{
			//Khởi tạo mảng danh sách phòng
			PHONG[] danhsachPhong = new PHONG[1000];

			// Gọi Hàm Nhập Phòng
			themPhong (danhsachPhong);
		}
	}
}
